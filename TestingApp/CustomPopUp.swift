//
//  CustomPopUp.swift
//  TestingApp
//
//  Created by Mac on 10/01/23.
//

import UIKit

class CustomPopUp: UIView {
        
    let dataArray = ["English", "Maths", "History", "German", "Science"]

    var viewcon : UIViewController?
    var toolBar = UIToolbar()
    var toolBarHeight = 44
    var popupheight : CGFloat = 200
    var bottomConstraint = NSLayoutConstraint()
    var pickerView = UIPickerView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    convenience init(_ vc : UIViewController) {
        self.init(frame: CGRect.zero)
        self.viewcon = vc
        self.initSubviews()
        

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func initSubviews() {
        self.viewcon!.view.addSubview(self)
        self.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewcon!.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewcon!.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1, constant: self.viewcon!.view.frame.width)
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: popupheight)
        self.bottomConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.viewcon!.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 294)
        self.viewcon!.view.addConstraints([leadingConstraint, trailingConstraint, widthConstraint, heightConstraint,self.bottomConstraint])
        
        self.toolBar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: Int(self.viewcon!.view.frame.width), height: toolBarHeight))
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        let flexibalspace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        self.toolBar.items = [done,flexibalspace,cancel]
        self.addSubview(self.toolBar)
        
        self.addSubview(self.pickerView)
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.frame = CGRect(x: 0, y: 44, width: self.viewcon!.view.frame.width, height: 200)
        self.bottomConstraint.constant = 294
        UIView.animate(withDuration: 2,
                       animations: {
            self.layoutIfNeeded()
            self.bottomConstraint.constant = 0
            
        }, completion: nil)
        
        
    }
    
    @objc func doneTapped(){
        self.bottomConstraint.constant = 294
    }
    
    @objc func cancel(){
        self.bottomConstraint.constant = 294
    }
}

extension CustomPopUp: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return dataArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       let row = dataArray[row]
       return row
    }
}
